<?php


namespace App\Controller;


use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AuthorController
{

    /**
     * @Route("/authors", name = "author")
     */


/*SELECT books.name AS b_n, authors.name AS a_n
  FROM authors_books
  INNER JOIN authors ON authors.id = authors_books.author_id
  INNER JOIN books ON books.id = authors_books.book_id
  WHERE authors_books.book_id = 1*/
    public function index(): Response
    {
        $mysqli= mysqli_connect("127.0.0.1", "deaddarkus", "bY8e21MBgtkSc726", "library");
        $result = $mysqli->query("SELECT * FROM authors");
        echo "<table border = 2>";
        echo "<tr>
                <th>n</th>
                <th>author name</th>
                </tr>";
        $i = 1;
        foreach ($result as $item) {
            echo "<tr>";
            echo "<td>".$i++."</td>";
            echo "<td>".$item['name']."</td>";
            echo "<td>"."<form action='authors/update/".$item['id']."' method='POST'>
                         <input type='submit' value='edit'>
                         </form>"."</td>";
            echo "</tr>";
        }
        echo "</table>";
        echo "<form action='authors/create' method='POST'>
                <br><br>
                <p>Input author name:</p>
                <input type='text' name='author_name'/>
                <input type='submit' value='add'>
              </form>";
        mysqli_close($mysqli);
        return new Response();
    }

    /**
     * @Route("/authors/create", name = "author_create")
     */
    public function create() : Response
    {
        $mysqli= mysqli_connect("127.0.0.1", "deaddarkus", "bY8e21MBgtkSc726", "library");
        $result = $mysqli->query("INSERT INTO `authors` (`id`, `name`) VALUES (NULL, '".$_POST['author_name']."'); ");
        mysqli_close($mysqli);
        header('Location: http://'.$_SERVER['HTTP_HOST'].'/authors');
        return new Response();
    }

    /**
     * @Route("/authors/update/{id}", name = "author_update")
     */
    public function update(string $id) : Response
    {
        echo "<form action='http://".$_SERVER['HTTP_HOST']."/authors/api/update/".$id."' method='POST'>
                <p>Input new author name:</p>
                <input type='text' name='author_name'/>
                <input type='submit' value='add'>
              </form>";
        return new Response();
    }
    /**
     * @Route("/authors/api/update/{id}", name = "author_api_update")
     */
    public function apiUpdate(string $id) : Response
    {
        $mysqli= mysqli_connect("127.0.0.1", "deaddarkus", "bY8e21MBgtkSc726", "library");
        $result = $mysqli->query("UPDATE authors SET name = '".$_POST['author_name']."' WHERE authors.id = ".$id.";");
        mysqli_close($mysqli);
        header('Location: http://'.$_SERVER['HTTP_HOST'].'/authors');
        return new Response();
    }
}