<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class BookController
{
    /**
     * @Route("/books", name = "book")
     */
    public function index(): Response
    {
        $order = 'id';
        if(!empty($_GET['order'])){ $order = $_GET['order']; };

        $direction = $this->getInverseDirection();

        $mysqli= mysqli_connect("127.0.0.1", "deaddarkus", "bY8e21MBgtkSc726", "library");
        $result = $mysqli->query("SELECT * FROM books ORDER BY ".$order." ".$direction);
        echo "<form action='books/create' method='POST'>
                <br><br>
                <input type='submit' value='add new book'>
                <br><br>
              </form>";
        echo "<table border = 2>";
        echo "<tr>
                    <th>n<form action='http://".$_SERVER['HTTP_HOST']."/books?order=id&direction=".$this->getInverseDirection()."' method='POST'><input type='submit' value='^'></form></th>
                    <th>book name<form action='http://".$_SERVER['HTTP_HOST']."/books?order=name&direction=".$this->getInverseDirection()."' method='POST'><input type='submit' value='^'></form></th>
                    <th>description<form action='http://".$_SERVER['HTTP_HOST']."/books?order=description&direction=".$this->getInverseDirection()."' method='POST'><input type='submit' value='^'></form></th>
                    <th>cover</th>
                    <th>year publication<form action='http://".$_SERVER['HTTP_HOST']."/books?order=year_publication&direction=".$this->getInverseDirection()."' method='POST'><input type='submit' value='^'></form></th>
                </tr>";
        $i = 1;
        foreach ($result as $item) {
            echo "<tr><form action='books/api/update/".$item['id']."' method='POST'>";
            echo "<td>".$i++."</td>";
            echo "<td><input type=\"text\" value='".$item['name']."' name='name'></td>";
            echo "<td><textarea type=\"textarea\" cols='40' rows='8' name='description'>".$item['description']."</textarea></td>";
            echo "<td><input type=\"text\" value='".$item['cover']."' name='cover'><br><br> <img src='".$item['cover']."' width='208' height='320'></td>";
            echo "<td><input type=\"text\" size='2' name='year_publication' value='".$item['year_publication']."'></td>";
            echo "<td>"."<input type='submit' value='edit'>"."</td>";
            echo "</form></tr>";
        }
        echo "</table>";

        return new Response();
    }

    /**
     * @Route("/books/create", name = "book_create")
     */
    public function create() : Response
    {
        echo "<form action='http://".$_SERVER['HTTP_HOST']."/books/api/create' method='POST'>".
            "book name:<br><input type=\"text\" name='name'><br>".
            "description:<br><textarea name='description'></textarea><br>".
            "link for cover: <br><input type=\"text\" name='cover'><br>".
            "year publication: <br><input type=\"text\" name='year_publication'><br>".
            "<br><input type='submit' value='Confirm'>".
        "</form>";
        return new Response();
    }
    /**
     * @Route("/books/api/create", name = "book_api_create")
     */
    public function apiCreate() : Response
    {
        $mysqli= mysqli_connect("127.0.0.1", "deaddarkus", "bY8e21MBgtkSc726", "library");
        $result = $mysqli->query("INSERT INTO `books` (`id`, `name`, `description`, `cover`, `year_publication`) VALUES (NULL, '".$_POST['name']."', '".$_POST['description']."', '".$_POST['cover']."', '".$_POST['year_publication']."') ");
        mysqli_close($mysqli);
        header('Location: http://'.$_SERVER['HTTP_HOST'].'/books');
        return new Response();
    }
    /**
     * @Route("/books/api/update/{id}", name = "book_api_update")
     */
    public function apiUpdate(string $id) : Response
    {
        $mysqli= mysqli_connect("127.0.0.1", "deaddarkus", "bY8e21MBgtkSc726", "library");
        $result = $mysqli->query("UPDATE `books` SET `name` = '".$_POST['name']."', `description` = '".$_POST['description']."', `cover` = '".$_POST['cover']."', `year_publication` = '".$_POST['year_publication']."' WHERE `books`.`id` = ".$id);
        mysqli_close($mysqli);
        header('Location: http://'.$_SERVER['HTTP_HOST'].'/books');
        return new Response();
    }

    public function getInverseDirection() : string
    {
        if(empty($_GET['direction'])) {
            return "DESC";
        }
        $direction = $_GET['direction'];
        switch($direction){
            case "DESC":
                return "ASC";
            case "ASC":
                return "DESC";
            default:
                return "DESC";
        }
    }
}